package ru.t1.kupriyanov.tm.api;

import ru.t1.kupriyanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    int getSize();

}